<?php
/* =========================================================================
    Nome: controle.php
    Autor: Raul Ribeiro Dos Santos
    Data Criação: 06/03/2020
    Descrição: Controlador do aplicativo
    Ultima Mudança: 06/03/2020
===========================================================================*/ 

    include_once "classes/models/marca.php";
    include_once "classes/models/produto.php";
    include_once "classes/models/especificacao.php";
    include_once "classes/daos/estoqueDao.php";
    include_once "classes/daos/marcaDao.php";
    include_once "classes/daos/produtoDao.php";
    include_once "classes/daos/precoDao.php";
    include_once "classes/daos/especificacaoDao.php";
    Class Controle{

        private $rota;
        
        public function imprimeRota(){
            return $this->rota;
        }

        public function defineRota($rota){
            $this->rota = $rota;
        }

        public function executarRota($data){
            if(isset($this->rota['acao'])){
                switch ($this->rota['acao']) {
                    
                    case 'produto':
                        $this->gerenciaProduto($data);
                    break;

                    case 'marca':
                        $this->gerenciaMarca($data);
                    break;
                    
                    case 'preco':
                        echo "take preco";
                    break;

                    case 'estoque':
                        echo "take estoque";
                    break;
                    
                    case 'especificacao':
                        echo "take especificacao";
                    break;
                        
                    case 'testes':
                        $produtoDao = new ProdutoDao();
                        $estoqueDao = new EstoqueDao();

                        $produto = new ProdutoModel();
                        $produto->setCodigoMarca($data['codigo_marca']);
                        $produto->setNome($data['nome']);
                        $produto->setDescricao($data['descricao']);
                        $produto->setEan($data['ean']);
                        $produto->setSku($data['sku']);

                        $lista = $produtoDao->listar();

                        foreach($lista as $estoque){
                            echo $estoque->getCodigo() . "- ";
                        }

                    break;
                    
                    default:
                        # code...
                    break;
                }
            }            
        }
        /*
            Função Responsavel por gerenciar Produtos
        */
        public function gerenciaProduto($data){
            if(isset($this->rota['metodo'])){

                $metodo = $this->rota['metodo'];
                $objProdutoDao = new ProdutoDao();
                switch($metodo){
                    case 'incluir':
                        $objProduto = new ProdutoModel();
                        $objProduto->setCodigo($data['codigo']);
                        $objProduto->setNome($data['nome']);
                        $objProduto->setEan($data['ean']);
                        $objProduto->setSku($data['sku']);
                        $objProduto->setDescricao($data['descricao']);
                        $objProduto->setCodigoMarca($data['codigoMarca']);

                        $objProdutoDao->incluir($objProduto);
                        echo "inserido";
                    break;
                    case 'editar':
                        $objProduto = new ProdutoModel();
                        $objProduto->setCodigo($data['codigo']);
                        $objProduto->setNome($data['nome']);
                        $objProduto->setEan($data['ean']);
                        $objProduto->setSku($data['sku']);
                        $objProduto->setDescricao($data['descricao']);
                        $objProduto->setCodigoMarca($data['codigoMarca']);

                        $objProdutoDao->editar($objProduto);
                        echo "Alterado inserido";
                    break;
                    case 'listar':
                        $retorno = $objProdutoDao->listar();
                        // echo json_encode($retorno);
                        var_dump($retorno);
                    break;
                    case 'obter':
                        $retorno = $objProdutoDao->obter($data['codigo']);
                        var_dump($retorno);
                    break;
                    case 'deletar':
                    break;
                    default:
                    break;
                }
            }else{
                echo "erro: Metodo não encontrado";
            }
        }

        public function gerenciaMarca($data){
            if(isset($this->rota['metodo'])){

                $metodo = $this->rota['metodo'];
                $objMarcaDao = new MarcaDao();
                switch($metodo){
                    case 'incluir':
                        $objMarca = new MarcaModel();
                        $objMarca->setCodigo($data['codigo']);
                        $objMarca->setDescricao($data['descricao']);
                        $objMarca->setLogo($data['logo']);
                        $objMarca->setNome($data['nome']);

                        $objMarcaDao->incluir($objMarca);
                        echo "inserido";
                    break;
                    case 'editar':
                        $objMarca = new MarcaModel();
                        $objMarca->setCodigo($data['codigo']);
                        $objMarca->setDescricao($data['descricao']);
                        $objMarca->setLogo($data['logo']);
                        $objMarca->setNome($data['nome']);

                        $objMarcaDao->editar($objMarca);
                        echo "Alterado inserido";
                    break;
                    case 'listar':
                        $retorno = $objMarcaDao->listar();
                        // echo json_encode($retorno);
                        var_dump($retorno);
                    break;
                    case 'obter':
                        $retorno = $objMarcaDao->obter($data['codigo']);
                        var_dump($retorno);
                    break;
                    case 'deletar':
                        $objMarcaDao->deletar($data['codigo']);
                    break;
                    default:
                    break;
                }
            }else{
                echo "erro: Metodo não encontrado";
            }
        }

        public function gerenciaPreco($data){
            if(isset($this->rota['metodo'])){

                $metodo = $this->rota['metodo'];
                $objPrecoDao = new PrecoDao();
                switch($metodo){
                    case 'incluir':
                        $objPreco = new PrecoModel();
                        $objPreco->setCodigo($data['codigo']);
                        $objPreco->setCodigoProduto($data['codigoProduto']);
                        $objPreco->setDataEntrada($data['dataEntrada']);
                        $objPreco->setDataLimite($data['dataLimite']);
                        $objPreco->setValor($data['valor']);

                        $objPrecoDao->incluir($objPreco);
                        echo "inserido";
                    break;
                    case 'editar':
                        $objPreco = new PrecoModel();
                        $objPreco->setCodigo($data['codigo']);
                        $objPreco->setCodigoProduto($data['codigoProduto']);
                        $objPreco->setDataEntrada($data['dataEntrada']);
                        $objPreco->setDataLimite($data['dataLimite']);
                        $objPreco->setValor($data['valor']);

                        $objPrecoDao->editar($objPreco);
                        echo "Alterado";
                    break;
                    case 'listar':
                        $retorno = $objPrecoDao->listar();
                        // echo json_encode($retorno);
                        var_dump($retorno);
                    break;
                    case 'obter':
                        $retorno = $objPrecoDao->obter($data['codigo']);
                        var_dump($retorno);
                    break;
                    case 'deletar':
                        $objPrecoDao->deletar($data['codigo']);
                    break;
                    default:
                    break;
                }
            }else{
                echo "erro: Metodo não encontrado";
            }
        }

        public function gerenciaEstoque($data){
            if(isset($this->rota['metodo'])){

                $metodo = $this->rota['metodo'];
                $objEstoqueDao = new EstoqueDao();
                switch($metodo){
                    case 'incluir':
                        $objEstoque = new EstoqueModel();
                        $objEstoque->setCodigo($data['codigo']);
                        $objEstoque->setCodigoProduto($data['codigoProduto']);
                        $objEstoque->setMaximo($data['maximo']);
                        $objEstoque->setMinimo($data['minimo']);
                        $objEstoque->setQuantidadeAtual($data['quantidadeAtual']);
                        $objEstoque->setQuantidadeReservada($data['quantidadeResevada']);

                        $objEstoqueDao->incluir($objEstoque);
                        echo "inserido";
                    break;
                    case 'editar':
                        $objEstoque = new EstoqueModel();
                        $objEstoque->setCodigo($data['codigo']);
                        $objEstoque->setCodigoProduto($data['codigoProduto']);
                        $objEstoque->setMaximo($data['maximo']);
                        $objEstoque->setMinimo($data['minimo']);
                        $objEstoque->setQuantidadeAtual($data['quantidadeAtual']);
                        $objEstoque->setQuantidadeReservada($data['quantidadeResevada']);

                        $objEstoqueDao->editar($objEstoque);
                        echo "Alterado";
                    break;
                    case 'listar':
                        $retorno = $objEstoqueDao->listar();
                        // echo json_encode($retorno);
                        var_dump($retorno);
                    break;
                    case 'obter':
                        $retorno = $objEstoqueDao->obter($data['codigo']);
                        var_dump($retorno);
                    break;
                    case 'deletar':
                        $objEstoqueDao->deletar($data['codigo']);
                    break;
                    default:
                    break;
                }
            }else{
                echo "erro: Metodo não encontrado";
            }
        }

        public function gerenciaEspecificacao($data){
            if(isset($this->rota['metodo'])){

                $metodo = $this->rota['metodo'];
                $objEspecificacaoeDao = new EspecificacaoDao();
                switch($metodo){
                    case 'incluir':
                        $objEspecificacao = new EspecificacaoModel();
                        $objEspecificacao->setCodigo($data['codigo']);
                        $objEspecificacao->setCodigoProduto($data['codigoProduto']);
                        $objEspecificacao->setDescricao($data['descricao']);
                        $objEspecificacao->setNome($data['nome']);

                        $objEspecificacaoeDao->incluir($objEspecificacao);
                        echo "inserido";
                    break;
                    case 'editar':
                        $objEspecificacao = new EspecificacaoModel();
                        $objEspecificacao->setCodigo($data['codigo']);
                        $objEspecificacao->setCodigoProduto($data['codigoProduto']);
                        $objEspecificacao->setDescricao($data['descricao']);
                        $objEspecificacao->setNome($data['nome']);

                        $objEspecificacaoeDao->editar($objEspecificacao);
                        echo "Alterado";
                    break;
                    case 'listar':
                        $retorno = $objEspecificacaoeDao->listar();
                        // echo json_encode($retorno);
                        var_dump($retorno);
                    break;
                    case 'obter':
                        $retorno = $objEspecificacaoeDao->obter($data['codigo']);
                        var_dump($retorno);
                    break;
                    case 'deletar':
                        $objEstoqueDao->deletar($data['codigo']);
                    break;
                    default:
                    break;
                }
            }else{
                echo "erro: Metodo não encontrado";
            }
        }
    }

?>