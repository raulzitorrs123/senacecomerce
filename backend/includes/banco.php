<?php
/* =========================================================================
    Nome: banco.php
    Autor: Raul Ribeiro Dos Santos
    Data Criação: 09/03/2020
    Descrição: Controlador do aplicativo
    Ultima Mudança: 09/03/2020
===========================================================================*/ 

    class Banco {

        private $db;

        function Banco(){
            $servidor   = "localhost";
            $usuario    = "root";
            $senha      = "";
            $banco      = "loja_informatica";
            
            $this->setDb(new mysqli($servidor, $usuario, $senha, $banco));
            
        }

        public function getDb(){
            return $this->db;
        }

        public function setDb($db){
            $this->db = $db;
        }
    }
?>