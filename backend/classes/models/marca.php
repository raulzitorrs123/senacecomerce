<?php
/* =========================================================================
    Nome: marca.php
    Autor: Raul Ribeiro Dos Santos
    Data Criação: 06/03/2020
    Descrição: Modelo de classe Marca
    Ultima Mudança: 06/03/2020
===========================================================================*/ 

class MarcaModel {

    private $codigo;
    private $descricao;
    private $logo;
    private $nome;
    
    public function getCodigo()
    {
        return $this->codigo;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;
    }
    
    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

}

?>