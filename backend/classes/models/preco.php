<?php
/* =========================================================================
    Nome: preco.php
    Autor: Raul Ribeiro Dos Santos
    Data Criação: 06/03/2020
    Descrição: Modelo de classe Preco
    Ultima Mudança: 06/03/2020
===========================================================================*/ 

class PrecoModel {

    private $codigo;
    private $codigoProduto;
    private $dataEntrada;
    private $dataLimite;
    private $valor;
    

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    public function getCodigoProduto()
    {
        return $this->codigoProduto;
    }

    public function setCodigoProduto($codigoProduto)
    {
        $this->codigoProduto = $codigoProduto;
    }

    public function getDataEntrada()
    {
        return $this->dataEntrada;
    }

    public function setDataEntrada($dataEntrada)
    {
        $this->dataEntrada = $dataEntrada;
    }
 
    public function getDataLimite()
    {
        return $this->dataLimite;
    }

    public function setDataLimite($dataLimite)
    {
        $this->dataLimite = $dataLimite;
    }

    public function getValor()
    {
        return $this->valor;
    }

    public function setValor($valor)
    {
        $this->valor = $valor;
    }
}

?>