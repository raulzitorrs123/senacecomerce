<?php
/* =========================================================================
    Nome: especificacoes.php
    Autor: Raul Ribeiro Dos Santos
    Data Criação: 06/03/2020
    Descrição: Modelo de classe Especificacoes
    Ultima Mudança: 06/03/2020
===========================================================================*/ 

class EspecificacaoModel {

    private $codigo;
    private $codigoProduto;
    private $descricao;
    private $nome;

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    public function getCodigoProduto()
    {
        return $this->codigoProduto;
    }

    public function setCodigoProduto($codigoProduto)
    {
        $this->codigoProduto = $codigoProduto;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }
}

?>