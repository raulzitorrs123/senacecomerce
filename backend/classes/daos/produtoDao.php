<?php
/* =========================================================================
    Nome: produtoDao.php
    Autor: Raul Ribeiro Dos Santos
    Data Criação: 10/03/2020
    Descrição: Dao dos Produtos
    Ultima Mudança: 10/03/2020
===========================================================================*/ 

include_once "classes/models/produto.php";
include_once "includes/banco.php";
//incluir classe de banco de dados

    class ProdutoDao{

        public function incluir($produto){
            $banco = new Banco();
            $query = "INSERT INTO 
                        produto 
                        (
                            codigo, 
                            nome, 
                            descricao, 
                            codigo_marca,
                            ean,
                            sku
                        ) VALUES ( 
                            NULL,
                            '". $produto->getNome() ."',
                            '". $produto->getDescricao() ."',
                            '". $produto->getCodigoMarca() ."',
                            '". $produto->getEan() ."',
                            '". $produto->getSku() ."'
                        )";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function listar(){
            $banco = new Banco();

            $query = "SELECT * FROM produto";

            $resultado = $banco->getDb()->query($query);

            if($resultado){
                $lista = array();
                while($dados = mysqli_fetch_array($resultado)){

                    $produto = new ProdutoModel();

                    $produto->setCodigo($dados['codigo']);
                    $produto->setNome($dados['nome']);
                    $produto->setDescricao($dados['descricao']);
                    $produto->setCodigoMarca($dados['codigo_marca']);
                    $produto->setEan($dados['ean']);
                    $produto->setSku($dados['sku']);

                    array_push($lista, $produto);
                }

                return $lista;
            }
        }
        public function editar($produto){
            $banco = new Banco();
            $query = "UPDATE 
                            produto  
                        SET
                            nome = '". $produto->getNome() ."',
                            descricao = '". $produto->getDescricao() ."',
                            codigo_marca = '". $produto->getCodigoMarca() ."',
                            ean = '". $produto->getEan() ."',
                            sku = '". $produto->getSku() ."'
                        WHERE
                            codigo = ". $produto->getCodigo() ."
                        ";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function obter($codigo){
            $banco = new Banco();
            $query = "SELECT * FROM produto WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $dados = mysqli_fetch_array($resultado);
                $produto = new ProdutoModel();
                $produto->setCodigo($dados['codigo']);
                $produto->setNome($dados['nome']);
                $produto->setDescricao($dados['descricao']);
                $produto->setCodigoMarca($dados['codigo_marca']);
                $produto->setEan($dados['ean']);
                $produto->setSku($dados['sku']);

                return $produto;

            }

            return $resultado;
        }
        public function deletar($codigo){
            $banco = new Banco();
            $query = "DELETE FROM produto WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
    }



?>