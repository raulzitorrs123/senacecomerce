<?php
/* =========================================================================
    Nome: EspecificacaoDao.php
    Autor: Raul Ribeiro Dos Santos
    Data Criação: 10/03/2020
    Descrição: Dao do Preco
    Ultima Mudança: 10/03/2020
===========================================================================*/ 

include_once "classes/models/especificacoes.php";
include_once "includes/banco.php";
//incluir classe de banco de dados

    class EspecificacaoDao{

        public function incluir($especificacoes){
            $banco = new Banco();
            $query = "INSERT INTO 
                        especificacoes 
                        (
                            codigo, 
                            codigo_produto, 
                            descricao, 
                            nome
                        ) VALUES ( 
                            NULL,
                            '". $especificacoes->getCodigoProduto() ."',
                            '". $especificacoes->getDescricao() ."',
                            '". $especificacoes->getNome() ."'
                        )";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function listar(){
            $banco = new Banco();

            $query = "SELECT * FROM especificacoes";

            $resultado = $banco->getDb()->query($query);

            if($resultado){
                $lista = array();
                while($dados = mysqli_fetch_array($resultado)){

                    $especificacoes = new especificacoesModel();

                    $especificacoes->setCodigo($dados['codigo']);
                    $especificacoes->setCodigoProduto($dados['codigo_produto']);
                    $especificacoes->setDescricao($dados['descricao']);
                    $especificacoes->setNome($dados['nome']);

                    array_push($lista, $especificacoes);
                }

                return $lista;
            }
        }
        public function editar($especificacoes){
            $banco = new Banco();
            $query = "UPDATE 
                            especificacoes  
                        SET
                            codigo_produto = '". $especificacoes->getCodigoProduto() ."',
                            descricao = '". $especificacoes->getDescricao() ."',
                            nome = '". $especificacoes->getNome() ."'
                        WHERE
                            codigo = ". $especificacoes->getCodigo() ."
                        ";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function obter($codigo){
            $banco = new Banco();
            $query = "SELECT * FROM especificacoes WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $dados = mysqli_fetch_array($resultado);
                $especificacoes = new EspecificacoesModel();
                $especificacoes->setCodigo($dados['codigo']);
                $especificacoes->setCodigoProduto($dados['codigo_produto']);
                $especificacoes->setDescricao($dados['descricao']);
                $especificacoes->setNome($dados['nome']);

                return $especificacoes;

            }

            return $resultado;
        }
        public function deletar($codigo){
            $banco = new Banco();
            $query = "DELETE FROM especificacoes WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
    }



?>