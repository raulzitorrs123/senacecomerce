<?php
/* =========================================================================
    Nome: index.php
    Autor: Raul Ribeiro Dos Santos
    Data Criação: 06/03/2020
    Descrição: Pagina Inicial do Sistema
    Ultima Mudança: 06/03/2020
===========================================================================*/ 

// Includes 
include_once "includes/controle.php";

// Tratativas
$objControle = new Controle();
// Controle
$objControle->defineRota($_GET);

$objControle->executarRota($_POST);
// Log

?>